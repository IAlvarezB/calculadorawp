﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Calculadora.Resources;

namespace Calculadora
{

    public partial class MainPage : PhoneApplicationPage
    {

        private readonly int NO_OPERACION = 0;
        private readonly int SUMA = 1;
        private readonly int RESTA = 2;
        private readonly int DIVISION = 3;
        private readonly int MULTIPLICAR = 4;
        private Boolean RESULTADO_TEMPORAL = false;
        private Boolean RESULTADO_TEMPORAL_ESP = false;


        private int HAY_OPERACION = 0;

        private int flag_igual;
        private float numero1;
        private float numero2;

        // Constructor
        public MainPage()
        {
            InitializeComponent();
            
            // Código de ejemplo para traducir ApplicationBar
            //BuildLocalizedApplicationBar();
        }

        private void boton_igual_Click(object sender, RoutedEventArgs e)
        {
            getResultado();
            boton_coma.IsEnabled = true;
            HAY_OPERACION = NO_OPERACION;
        }

        private void boton_operacion(object sender, RoutedEventArgs e)
        {
            
                if (HAY_OPERACION != NO_OPERACION )
                {
                    getResultado();
                    RESULTADO_TEMPORAL = true;
                    getOperacion(sender);
                
                }
                else
                {
                    getOperacion(sender);
                    RESULTADO_TEMPORAL = false;
                    cuadro_resultado.Text = "0";
                }

                boton_coma.IsEnabled = true;
                flag_igual = 0;
            
            
        }

        private void boton_limpiar(object sender, RoutedEventArgs e)
        {
            reset();
        }


        private void boton_evento(object sender, RoutedEventArgs e)
        {
            Button btnSender = (Button)sender;

            //En el caso que venga de pulsar un igual, flag_igual sera 1, en ese caso se resetea todo.
            if (flag_igual == 1)
            {
                reset();
                flag_igual = 0;
            }
            
            //Para que no se acumulen 0 a la izquierda.
            //En el caso que se muestre el resultado temporal de la operacion.
            if (cuadro_resultado.Text.Equals("0") || RESULTADO_TEMPORAL || RESULTADO_TEMPORAL_ESP)
            {
                RESULTADO_TEMPORAL = false;
                RESULTADO_TEMPORAL_ESP = false;
                cuadro_resultado.Text = btnSender.Content.ToString();
            }
            else
            {
                //Se van sumando todos los botones de numeros que va introducciendo el usuario.
                cuadro_resultado.Text += btnSender.Content.ToString();
            }

            //Control de la coma
            if (btnSender.Content.ToString().Equals("."))
            {
                boton_coma.IsEnabled = false;
            }

            

        }

        private void reset()
        {

            //Se resetea todo
            cuadro_resultado.Text = "0";
            HAY_OPERACION = 0;
            boton_coma.IsEnabled = true;
            RESULTADO_TEMPORAL = false;
            RESULTADO_TEMPORAL_ESP = false;
            numero1 = 0;
            numero2 = 0;
        }

        //Obtiene la operacion del boton y se almacena en la variable HAY_OPERACION 
        //y se setea el numero1 con el que se va a hacer la operacion
        private Boolean getOperacion(object sender)
        {
            Button btnSender = (Button)sender;
            String value = btnSender.Content.ToString().ToLowerInvariant();

            
            numero1 = float.Parse(cuadro_resultado.Text);
            

            switch (value)
            {
                case "x":
                 //   numero1 =  float.Parse(cuadro_resultado.Text);
                    HAY_OPERACION = MULTIPLICAR;
                    return true;
                case "-":
                  //  numero1 = float.Parse(cuadro_resultado.Text);
                    HAY_OPERACION = RESTA;
                    return true;
                case "/":
                   // numero1 = float.Parse(cuadro_resultado.Text);
                    HAY_OPERACION = DIVISION;
                    return true;
                case "+":
                   // numero1 = float.Parse(cuadro_resultado.Text);
                    HAY_OPERACION = SUMA;
                    return true;
                case "ce":
                    cuadro_resultado.Text = "";
                    HAY_OPERACION = NO_OPERACION;
                    return true;

                default:
                    return false;
            }

        }


        //Se obtiene el resultado segun la operacion almacenada en HAY_OPERACION.
        private void getResultado()
        {
            flag_igual = 1;
            
            switch (HAY_OPERACION)
            {
                case 0:
                    numero1 = float.Parse(cuadro_resultado.Text);
                    cuadro_resultado.Text = numero1.ToString();
                    break;

                case 1:

                    if (!RESULTADO_TEMPORAL) {
                        numero2 = float.Parse(cuadro_resultado.Text);
                        cuadro_resultado.Text = (numero1 + numero2).ToString();
                    }              
                    break;

                case 2:

                    if (!RESULTADO_TEMPORAL)
                    {
                        numero2 = float.Parse(cuadro_resultado.Text);
                        cuadro_resultado.Text = (numero1 - numero2).ToString();
                    }
                    break;

                case 3:
                    if (!RESULTADO_TEMPORAL)
                    {
                        numero2 = float.Parse(cuadro_resultado.Text);
                        cuadro_resultado.Text = (numero1 / numero2).ToString();
                    }
                    break;
                   
                case 4:
                    if (!RESULTADO_TEMPORAL)
                    {
                        numero2 = float.Parse(cuadro_resultado.Text);
                        cuadro_resultado.Text = (numero1 * numero2).ToString();
                    }
                    break;

                default:

                    break;
            }

            
            
        }

        //Metodo encargardo de la orientacion del dispositivo y que se muestre correctamente.
        private void PhoneApplicationPage_OrientationChanged( object sender, OrientationChangedEventArgs e)
        {
            // Vertical
            if ((e.Orientation & PageOrientation.Portrait) == (PageOrientation.Portrait))
            {
                Grid.SetRow(panelResultado, 0);
                Grid.SetColumn(panelResultado, 0);
                Grid.SetRow(panelNumeros, 1);
                Grid.SetColumn(panelNumeros, 0);

                //Mostramos la columna con los nuevos operandos.
                panelOpcional.Visibility = Visibility.Collapsed;
                           
            }
            // Horizontal
            else
            {
                Grid.SetRow(panelResultado,0);
                Grid.SetColumn(panelResultado, 1);
                Grid.SetRow(panelNumeros, 1);
                Grid.SetColumn(panelNumeros, 1);
                Grid.SetColumn(panelOpcional, 0);

                FirstColumn.Width = new GridLength(1, GridUnitType.Star);
                SecondColumn.Width = GridLength.Auto;
                //Escondemos la columna con los nuevos operandos.
                panelOpcional.Visibility = Visibility.Visible;
               
            }
        }

        //Metodo que se encarga de las operaciones especiales cuando el dispositivo esta en landscape.
        private void boton_OperacionEspecial(object sender, RoutedEventArgs e)
        {
            Button btnSender = (Button)sender;
            String value = btnSender.Content.ToString().ToLowerInvariant();


            if (HAY_OPERACION != 0)
            {
                numero2 = float.Parse(cuadro_resultado.Text);
            }
            else
            {
                numero1 = float.Parse(cuadro_resultado.Text);
            }

            switch (value)
            {
                case "raiz":
                    
                    cuadro_resultado.Text = Math.Sqrt(double.Parse(cuadro_resultado.Text)).ToString();
                    RESULTADO_TEMPORAL_ESP = true;
                    break;

                case "sin":
                    cuadro_resultado.Text = Math.Sin(double.Parse(cuadro_resultado.Text)).ToString();
                    RESULTADO_TEMPORAL_ESP = true;
                    break;

                case "log":
                    cuadro_resultado.Text = Math.Log10(double.Parse(cuadro_resultado.Text)).ToString();
                    RESULTADO_TEMPORAL_ESP = true;
                    break;

                case "tan":
                    cuadro_resultado.Text = Math.Tan(double.Parse(cuadro_resultado.Text)).ToString();
                    RESULTADO_TEMPORAL_ESP = true;
                    break;

                case "pi":
                    cuadro_resultado.Text = Math.PI.ToString();
                    RESULTADO_TEMPORAL_ESP = true;
                    break;

                case "%":
                    cuadro_resultado.Text = (float.Parse(cuadro_resultado.Text)/100).ToString();
                    RESULTADO_TEMPORAL_ESP = true;
                    break;

                case "cos":
                    cuadro_resultado.Text = Math.Cos(float.Parse(cuadro_resultado.Text)).ToString();
                    RESULTADO_TEMPORAL_ESP = true;
                    break;

                case "ln":
                    cuadro_resultado.Text = Math.Log(float.Parse(cuadro_resultado.Text)).ToString();
                    RESULTADO_TEMPORAL_ESP = true;
                    break;

                case "arctan":

                    cuadro_resultado.Text = Math.Atan(float.Parse(cuadro_resultado.Text)).ToString();
                    RESULTADO_TEMPORAL = true;
                    break;

                case "e":

                    cuadro_resultado.Text = Math.E.ToString();
                    RESULTADO_TEMPORAL_ESP = true;
                    break;
                
                default:
                    break;
            }
        }
    }
}